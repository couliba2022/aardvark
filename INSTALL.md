# Install Aardvark

This describes how to install Aardvarks on a Kubernetes/OpenShift cluster.
Prerequisites is that [Tekton](https:/(/tekton.dev/)) is installed on the cluster,
in OpenShift this is the OpenShift Pipeline Operator.

First create a project(namespace in plain Kubernetes):
```shell
oc new project aardvark
```

Take the files in [examples directory](examples) and modify them with your
organizations secrets.
* [docker-registry-secret-example.yaml](examples/docker-registry-secret-example.yaml) contains login for the Docker registry the container images are pushed too.
* [gitlab-access-token-example.yaml](examples/gitlab-access-token-example.yaml) contains the
token for the user with the rights to read and write to all projects. This is used to manage the API.
* [gitlab-ssh-example.yaml](examples/gitlab-ssh-example.yaml) contains the
ssh key for the user with the rights to read and write to all projects. This is used to checkout projects.
* [gitlab-webhook-secret.yaml](examples/gitlab-webhook-secret.yaml) contains the secret
sent together with the webhooks from GitLab to Aardvark to trigger builds etc. You shall
share this with all users that shall be able to set up project to use Aardvark.

Once you have modified the files with secrets apply them one by one using
`kubectl apply -f FILENAME`

Deploy the rest of Aardvark:
 `kubectl apply -k kustomize/overlays/prod`

After the installation you can get the webhook-url via:
```shell
kubectl -n aardvark get route el-aardvark-listener -o jsonpath="https://{.spec.host}"
```
Share this URL and the webhook secret with your users of Aardvark.
These are used when setting up webhooks in GitLab.


There is now a full build pipeline setup to build and deploy project structured like
the [demo project](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo).

The address to the webhook is shown via `kubectl get route`

If you want to have a dedicated test/develop environment to improve Aardvark. Create a project named `aardvark-test` and deploy the Kustomize overlay for develop `kustomize/overlays/develop`.
