# Remote cluster deployment

To deploy on other cluster than we are building our container image a pull strategy is used.
ArgoCD or OpenShift GitOps must be installed on the remote cluster.
ArgoCD will frequently pull the infra-repository of your project to see if there are any
changes and if the deployment on the cluster matches. If deployment on
the cluster does not match git, the deployment on the cluster will
be updated.

Ensure you have made the deployment work on the same cluster as you run
Tekton following
[instructions for deployment](docs/usage-deploy.md). The same Kustomize directory structure
is used, just other overlays are deployed. In this example `prod`.

Make sure you are logged in on the production cluster.

Create the projects in OpenShift that you want to use, example:
`oc new-project aardvark-demo-prod`
In the project/namespace do also apply [argocd-deployer-access-to-project.yaml]. This gives the ArgoCD the permission to deploy in the project.
By default it get permission to manage the following resources:

* serviceaccount
* service
* persistentvolume
* deployments
* route
Apply it with `kubectl apply -f https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/argocd-deployer-access-to-project.yaml`.

Last thing is to tell ArgoCD what repository, directory, and branch to deploy and where. We do this creating an `Application` resource for ArgoCD.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  # Name of your Application, suggestion is to use same as the project created
  # to avoid conflicts.
  name: aardvark-demo-prod
  namespace: openshift-gitops
spec:
  destination:
    # This namespace shall be same as the OpenShift project you created.
    namespace: aardvark-demo-prod
    server: https://kubernetes.default.svc
  project: default
  source:
    # URL to the git repo to deploy. Of the two repos you are working with it shall be the infra.
    repoURL: https://gitlab.com/arbetsformedlingen/devops/aardvark-demo-infra.git
    # Path to the overlay to deploy
    path: kustomize/overlays/prod
    # Branch to use, most often main or master.
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
    # Do not let ArgoCD create namespace, it will not get the right permissions.
    - CreateNamespace=false
```

Apply the file to the cluster: `kubectl apply -f argocd-prod.yaml`.

ArgoCD will automatically check the Git-repository, discover that cluster is not in sync and
deploy. Do `kubectl -n openshift-gitops get Application` to see the status of the deployment.

```
NAME                 SYNC STATUS   HEALTH STATUS
aardvark-demo-prod   Synced        Healthy
```

Once sync status is synced and health status healthy, you can access
your application.
