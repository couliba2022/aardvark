# Usage of deploy pipeline

Before following this document, ensure your project builds
correctly and is set up according to the
[build instructions](usage-build.md).

Once you have the build pipeline working create another
repository for instructions how to deploy. Name the repository
the same as your code repository and add a suffix `-infra`.
Default branch of the repository must be `main` or `master`.

Create a kustomize directory structure looking like this in the repository:

```text
.
+--kustomize/
   +--base/
      +--kustomization.yaml
      +--Rest of yaml to deploy app.
   +--overlays/
      +--develop/
         +--kustomization.yaml
         +--other yaml might need
      +--test/
         +--kustomization.yaml
         +--other yaml might need
      +--prod/
         +--kustomization.yaml
         +--other yaml might need
```

The [Kustomize site](https://kustomize.io/) describes how to
write your Kustomize files. You can use
the [demo project](https://gitlab.com/m.runesson/aardvark-demo-infra)
as an initial template, it probably suits a first deploy.

The `base` directory contains Kubernetes manifests that is common for
most environments.
Each environment (develop, test, production) has its own overlay, stored
in a subdirectory under `overlays` directory. The base and one overlay are
merged when applying.

To see the generated manifest from an overlay you can run:
`kubectl kustomize kustomize/overlays/develop`. Change the directory
to output from another overlay.

The *develop* overlay is used to deploy the `main`/`master` branch. This is deployed every time someone pushes or merge to the default branch. This overlay might also adding some mocks of external
systems your service is dependent on to make run more isolated and easier
to verify functionality.

The *test* overlay is deployed when a commit is tagged with a git tag *test*. If desired, overlay can be called *staging* and a tag *staging* can be used instead. Different teams prefer different namings.

The *prod* overlay works in the same way as *test* but with the intention to deploy to production.

Any of the overlays can be omitted.

Remember to not store secrets in git.

Below is a minimal `kustomization.yaml`, in this case for a
test environment.

```yaml
#cluster: test
bases:
  - ../../base
namespace: aardvark-demo-test
images:
  - name: docker-images.jobtechdev.se/aardvark-demo/aardvark-demo:latest
    newName: docker-images.jobtechdev.se/aardvark-demo/aardvark-demo
    newTag: XXXXXXXX
```

First row indicates what cluster to deploy to. One might have several
clusters. Default setup is that the Aardvark pipeline run on a test
cluster. The example indicates the overlay shall be installed on that
cluster. If you want to deploy to other clusters, read about
[remote deploy](remote-deploy.md).

Fourth line set the namespace. A dedicated namespace per overlay is recommended.

The first line in the `images` section correspond to image name written in the
base layer, including the image tag. This is the image that we want to change to
use our recently build or released image.
`newName` is what the new name shall be, most often same as name in base layer, without tag.
Last line, `newTag` can the value be whatever. This value will be changed
by Aardvarks build and release pipelines to the commit sha of image to be used. As a
start use a tag you now you have an image for.

Often one need to set up some secrets, these shall not be stored i GitLab.
We neither want to not give Aardvark to many permissions in the cluster.
Therefore one must setup namespaces/projects and secrets manually in the cluster.
This normally only done once per project.

Create the projects in OpenShift that you want to use, example:
`oc new-project aardvark-demo-test`
In the project/namespace do also apply [aardvark-deploy-bot-access-to-project.yaml].
This gives the Aardvark permissions to deploy in the project.
By default it get permission to manage the following resources:

* serviceaccount
* service
* persistentvolume
* deployments
* route

If your manifest using other resources, Aardvark must have the right to
deploy those too.
Apply it with `kubectl apply -f https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/aardvark-deploy-bot-access-to-project.yaml`.

Finally, it is time to set up the webhook in the infra repository to
trigger Aardvark to deploy every time it is updated.

Go to the GitLab page for the repository containing the infrastructure code.
Under *Settings*, select *Webhooks*.
In the field *url* set the webhook url you got from your administrator.

Field *Secret Token* is the webhook secret you got from your administrator.

Mark the checkboxes to trigger:

* Push events

Finally click *Add webhook*.

Test your setup selecting *Test* and *Push event* for the webhook just created.
It triggers a deploy pipeline and results in a deployment into the develop
environment.

It might fail if the tag you wrote in *newTag* in the kustomization file
does not exist. If so, do a small change in your code repo and push it to
the default branch to automatically trigger a build and then a deploy.
You shall see that the build have changed your infra repository.

Once you have your develop-environment deploying automatically, move on to
ensure test and production works too. If you need to deploy on remote
clusters, see [separate documentation](remote-deploy.md).
