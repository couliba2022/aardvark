# Git tags for release

Tags in git are labels set on a commit. One tag can only be set on one commit at a time.
You can of course have multiple tags within a repository and a single commit can also
have multiple tags.

To tag the last commit in your current branch with the tag `test`, do:

```shell
git tag test
```

To tag another commit, you must know the commit id. It can easily be found with `git log`. Example:

```shell
git tag test 8f1e42bc95c0002e2527c091063de7dc6172450a
```

To make the tags visible in GitLab, add the argument `--tags` to your push command. Example:

```shell
git push --tags
```

In the case of Aardvark this triggers a test-release and deployment.

**Note:** When working with tags in git it is important to use the flag ``--tags``
when doing push, pull and fetch to get the tags included in the request.

As mentioned earlier a tag can only exist on one commit. If you have an active test release
and want to do a new, you must delete(flag `-d`) the old `test` tag first. Example to make your current
commit to be the new test release:

```shell
git tag -d test                 # Delete tag in local repository.
git push --delete origin test   # Delete tag in remote repository
git tag test                    # Tag in local repository
git push --tags                 # Push new tag to remote repository
```

The delete flag(`-d`) deletes the tag from the git repository no mather if it is set on your current
commit or any one else.

As you realise, it is not easy to see historical releases. It might be valuable to always combine the
tags we use for releases(`test`, `staging`, and `prod`) with another permanent tag, for instance suffix
with date. Example:

```shell
git tag prod
git tag prod-20210606
```

## Other useful things with tags

If you want to checkout a tag as a new branch, for instance a previous release, you do that with checkout:

```shell
git checkout tags/prod-20210606 -b prod-20210606-fix-bug
```

List all tags in the repository:

```shell
git tag
```

Get the commit id of a tag:

```shell
git rev-list -n 1 tags/prod-20210606
```
