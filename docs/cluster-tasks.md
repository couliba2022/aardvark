# Cluster tasks

Cluster tasks in Tekton are the same as normal tasks, but they are available in 
all namespaces of the cluster. This repository provides a couple of cluster tasks
that can be useful in other pipelines too.
They are all prefixed with `jobtech-`

Yaml including usage description can be found [here](../kustomize/base/tekton-clustertasks) 
or do `kubectl get clustertasks | grep '^jobtech-'`.
