#!/bin/bash


if ! git rev-parse --git-dir > /dev/null 2>&1; then
    echo "Directory must be revision controlled by git."
    exit 2
fi

if [ ! -d kustomize/overlays ] ; then
    echo "No kustomize/overlays directory. Are you in the infra repository? Directory structure must follow Aardwark opinions."
    exit 3
fi

 if ! git diff --quiet 2>/dev/null >&2 ; then
    echo "Commit or stash your changes. Cannot diff on a dirty repository."
    git status
    exit 4
fi


if ! git ls-files --other --directory --exclude-standard | sed q1 > /dev/null ; then
    echo "There are untracked files. Please remove those."
    git ls-files --other --directory --exclude-standard
    exit 5
fi


git_branch=$(git branch --show-current)

if [ $git_branch = "main" ] || [ $git_branch = "master" ] ; then
    echo "You must not be on the default branch(main or master)."
    exit 6
fi

git_default_branch=$(git branch | grep "^  \(main\|master\)$" | cut -b3- | head -n 1)
outdir=$(mktemp -d diff-XXXX)

echo "Output directory: $outdir"

mkdir $outdir/branch
mkdir $outdir/main
mkdir $outdir/diff

for i in kustomize/overlays/* ; do
    e=$(echo $i | cut -d/ -f3)
    kubectl kustomize $i > $outdir/branch/$e.yaml
done

git checkout $git_default_branch
git pull

for i in kustomize/overlays/*; do
    e=$(echo $i | cut -d/ -f3)
    kubectl kustomize $i > $outdir/main/$e.yaml
done

git checkout $git_branch

diff=0
echo && echo
for i in kustomize/overlays/*; do
    e=$(echo $i | cut -d/ -f3)
    if diff $outdir/main/$e.yaml $outdir/branch/$e.yaml > $outdir/diff/$e.yaml ; then
        echo "$e: No change between $git_default_branch and $git_branch branch."
        rm $outdir/diff/$e.yaml
    else
        echo "$e: Difference between $git_default_branch and $git_branch branch. See diff in $outdir/diff/$e.yaml"
        diff=1
    fi
done

[ $diff -eq 0 ] && rm -rf $outdir

exit $diff
