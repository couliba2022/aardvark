# Aardvark

This project provides a common build and deployment pipeline as a service for
projects. It builds a image container and deploys it to Kubernetes. Intended
use is for smaller project to make it easy to set up a fully
automated build and deploy. [This presentation](https://docs.google.com/presentation/d/e/2PACX-1vRqEmkly2UA0oxrctfKYCaykAJpgscN2J7RPbhAs_rt1VX3PMZe8MM9jmvznqQ3sQRn57X8zR_p521d/pub?start=true&loop=false&delayms=3000)
illustrates how a developer can use Aardvark after it is set up.

The pipeline is highly opinionated. One can either use only the
build pipeline or both build and deployment pipeline.

This repository do also contains a set of common [cluster tasks](docs/cluster-tasks.md).

## Pre-requirements

To be able to use this to build image containers a project must:

* Have a `Dockerfile` in the root of the repository
* The container must be possible to build without extra
  arguments such as environment variables.
* Git repository shall be named with lower case, numbers and dashes.

Output will be:

* A container pushed to image registry tagged with branch and short
  git commit-sha.
* Container is named `repository/repository`
* When a commit in git is tagged, the same tag will be added to the
  image in the registry.

To enable deployment there must be a sibling repository named the same but with a suffix `-infra`.
The pipeline assumes this contains Kubernetes manifest using
[Kustomize](https://kustomize.io/) to deploy the
image.
Deployment can automatically happen when the default branch is updated
or a tag named `prod`, `prod-stdby`, `test`, or `staging` is set.
*Note* that there are one setup for
[deploy on the same cluster as build happens](docs/usage-deploy.md)
 and another to [deploy on remote clusters](docs/remote-deploy.md).

Requirements for this to happen are:

* There is a directory named `kustomize`. It contains Kustomize
  structure according to the [documentation](docs/usage-deploy.yaml).

Aardvark is tested on an OpenShift cluster. If using OpenShift all `kubectl` commands can be replaced with `oc`, or do `alias kubectl=oc` if not already done.

## Usage

Start with reading the description of the [process](docs/process.md)
Aardvark is expecting to be followed. This also provides an overview
of what will happen.

To set up your project to be built by Aardvark follow the
[instructions for build](docs/usage-build.md).

Once build is working, follow the
[instructions for deployment](docs/usage-deploy.md) to automate
the deployment within the same cluster.

To deploy in another cluster follow the instructions for
[remote deploy](docs/remote-deploy.md).

## Hello World example project

There are two demo repositories to showcase how a project
using Aardvark must be structured.
[First repository](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo)
contains source code for a very simple Python Flask app. It has a
`Dockerfile` to package a container image.
[The second repository](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo-infra),
is the adjacent infrastructure repository containing Kubernetes
manifest using [Kustomize](https://kustomize.io/). At the moment
this only shows deploy on the build cluster.

## Naming

The project is called [Aardvark](https://en.wikipedia.org/wiki/Aardvark)
because of its contribution of building homes for many spices of the african
bush. In the same way this project aims to build "homes" for many projects.
